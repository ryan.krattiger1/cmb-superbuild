set(smtkprojectmanager_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND smtkprojectmanager_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  smtkprojectmanager_rpaths
  "${smtkprojectmanager_rpaths}")

superbuild_add_project(smtkprojectmanager
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost cxx11 libarchive paraview qt5 smtk
  DEPENDS_OPTIONAL python python2 python3
  CMAKE_ARGS
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:STRING=${smtkprojectmanager_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib)
