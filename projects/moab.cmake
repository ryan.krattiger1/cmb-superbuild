superbuild_add_project(moab
  DEPENDS eigen hdf5 netcdf
  DEPENDS_OPTIONAL cxx11
  CMAKE_ARGS
    -Wno-dev
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_CXX_VISIBILITY_PRESET:STRING=hidden
    -DCMAKE_VISIBILITY_INLINES_HIDDEN:BOOL=ON
    -DENABLE_BLASLAPACK:BOOL=OFF
    -DMOAB_HAVE_EIGEN:BOOL=ON
    -DMOAB_USE_SZIP:BOOL=ON
    -DMOAB_USE_CGM:BOOL=OFF
    -DMOAB_USE_CGNS:BOOL=OFF
    -DMOAB_USE_MPI:BOOL=OFF
    -DMOAB_USE_HDF:BOOL=ON
    -DENABLE_HDF5:BOOL=ON # also required to get hdf5 support enabled
    -DMOAB_USE_NETCDF:BOOL=ON
    -DENABLE_NETCDF:BOOL=ON # also required to get ncdf/exodus enabled
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>
    -DENABLE_TESTING:BOOL=OFF)

superbuild_apply_patch(moab disable-fortran
  "Disable use of fortran")

superbuild_apply_patch(moab export-include-dir
  "Set MOAB and iMesh targets to export their installed include directories")

superbuild_apply_patch(moab find-hdf5-default-path
  "When using CMake to look for HDF5, use the default path")

superbuild_apply_patch(moab add-alternate-name-for-triangle
  "Add ExodusII standard label TRIANGLE for triangle elements")

superbuild_apply_patch(moab ext-deps
  "Re-find external dependencies when finding MOAB")

# By default, linux and os x cmake looks in <INSTALL_DIR>/lib/cmake for
# things. On windows, it does not. So, we set MOAB_DIR to point to the
# location of MOABConfig.cmake for everyone.
superbuild_add_extra_cmake_args(
  -DMOAB_DIR:PATH=<INSTALL_DIR>/lib/cmake/MOAB)
