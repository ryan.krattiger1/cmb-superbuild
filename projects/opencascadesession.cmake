set(extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

superbuild_add_project(opencascadesession
  DEVELOPER_MODE
  DEBUGGABLE
  DEPENDS boost cxx11 libarchive occt paraview qt5 smtk
  DEPENDS_OPTIONAL python python2 python3
  CMAKE_ARGS
    ${extra_cmake_args}
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DENABLE_PYTHON_WRAPPING:BOOL=${python_enabled})
