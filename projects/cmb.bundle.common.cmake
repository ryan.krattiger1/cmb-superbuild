# Consolidates platform independent stub for cmb.bundle.cmake files.

################################################################################
# Version information
################################################################################

include(cmb-version)
include(paraview-version)
include(smtk-version)

if (NOT DEFINED package_version_name)
  set(package_version_name cmb)
endif ()

################################################################################
# Package metadata
################################################################################

if (NOT DEFINED package_suffix)
  set(package_suffix "${CMB_PACKAGE_SUFFIX}")
endif ()

set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR ${${package_version_name}_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${${package_version_name}_version_minor})
set(CPACK_PACKAGE_VERSION_PATCH ${${package_version_name}_version_patch}${${package_version_name}_version_suffix})
if (package_suffix)
  set(CPACK_PACKAGE_VERSION_PATCH ${CPACK_PACKAGE_VERSION_PATCH}-${package_suffix})
endif ()

if (NOT DEFINED package_filename)
  set(package_filename "${CMB_PACKAGE_FILE_NAME}")
endif ()

if (package_filename)
  set(CPACK_PACKAGE_FILE_NAME "${package_filename}")
else ()
  set(CPACK_PACKAGE_FILE_NAME
    "${cmb_package_name}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
endif ()

# Set the license file.
set(CPACK_RESOURCE_FILE_LICENSE
  "${superbuild_source_directory}/License.txt")

################################################################################
# Program selection
################################################################################

list(SORT cmb_programs_to_install)
list(REMOVE_DUPLICATES cmb_programs_to_install)

set(paraview_executables)
if (cmb_install_paraview_server)
  list(APPEND paraview_executables
    pvserver
    pvdataserver
    pvrenderserver)
endif ()
if (cmb_install_paraview_python)
  list(APPEND paraview_executables
    pvbatch
    pvpython)
endif ()

################################################################################
# Plugin file detection
################################################################################

function (extract_plugin_list varname pluginxml)
  file(STRINGS "${pluginxml}"
    plugin_lines
    REGEX "name=\"[A-Za-z0-9]+\"")
  set(plugin_names)
  foreach (plugin_line IN LISTS plugin_lines)
    string(REGEX REPLACE ".*name=\"\([A-Za-z0-9]+\)\".*" "\\1" plugin_name "${plugin_line}")
    list(APPEND plugin_names
      "${plugin_name}")
  endforeach ()
  set("${varname}"
    ${plugin_names}
    PARENT_SCOPE)
endfunction ()

list(APPEND projects_with_plugins paraview)
set(paraview_plugin_dir "${plugin_dir}/paraview-${paraview_version}/plugins")
set(paraview_plugin_files
  "${paraview_plugin_dir}/paraview.plugins.xml")

list(APPEND projects_with_plugins smtk)
set(smtk_plugin_dir "${plugin_dir}/smtk-${smtk_version}.${smtk_version_patch}")
set(smtk_plugin_files
  "${smtk_plugin_dir}/smtk.extensions.xml"
  "${smtk_plugin_dir}/smtk.paraview.xml")

list(APPEND projects_with_plugins smtkprojectmanager)
set(smtkprojectmanager_plugin_files
  "${smtk_plugin_dir}/smtk.projectmanager.xml")

list(APPEND projects_with_plugins smtkresourcemanagerstate)
set(smtkresourcemanagerstate_plugin_files
  "${smtk_plugin_dir}/smtk.readwriteresourcemanagerstate.xml")

list(APPEND projects_with_plugins rggsession)
set(rggsession_plugin_files
  "${smtk_plugin_dir}/smtk.rggsession.xml")

list(APPEND projects_with_plugins opencascadesession)
set(opencascadesession_plugin_files
  "${smtk_plugin_dir}/smtk.opencascadesession.xml")

if (modelbuilder_enabled AND "modelbuilder" IN_LIST cmb_programs_to_install)
  list(APPEND projects_with_plugins cmb)
  set(cmb_plugin_dir "${plugin_dir}/cmb-${cmb_version}")
  list(APPEND cmb_plugin_files
    "${cmb_plugin_dir}/cmb.xml")
  set(cmb_plugin_omit
    # This is a static plugin.
    cmbPostProcessingModePlugin)
endif ()

if ("aevaCMB" IN_LIST cmb_programs_to_install)
  list(APPEND smtk_plugin_omit
    smtkDelaunayPlugin
    smtkMeshPlugin
    smtkMeshSessionPlugin
    smtkOscillatorSessionPlugin
    smtkPVMeshExtPlugin
    smtkPolygonSessionPlugin
    smtkVTKSessionPlugin)

  list(APPEND projects_with_plugins aevasession)
  set(aevasession_plugin_dir "${plugin_dir}/aeva-session-1.0")
  set(aevasession_plugin_files
    "${aevasession_plugin_dir}/aeva.session.xml")

  list(APPEND projects_with_plugins aeva)
  set(aeva_plugin_dir "${plugin_dir}/aeva-${aeva_version}")
  set(aeva_plugin_files
    "${aeva_plugin_dir}/aeva.xml")
  set(aeva_plugin_omit
    # This is a static plugin.
    aevaPostProcessingModePlugin)
endif ()

set(all_plugin_files)
foreach (project IN LISTS projects_with_plugins)
  if (NOT ${project}_enabled)
    continue ()
  endif ()

  foreach (plugin_file IN LISTS "${project}_plugin_files")
    list(APPEND all_plugin_files "${plugin_file}")
    extract_plugin_list("plugins_${plugin_file}" "${superbuild_install_location}/${plugin_file}")

    if (DEFINED "${project}_plugin_omit")
      list(REMOVE_ITEM "plugins_${plugin_file}"
        ${${project}_plugin_omit})
    endif ()
  endforeach ()
endforeach ()

set(cmb_additional_libraries)
foreach (boost_lib IN LISTS BOOST_ADDITIONAL_LIBRARIES)
  list(APPEND cmb_additional_libraries boost_${boost_lib})
endforeach ()

################################################################################
# Python modules
################################################################################

set(cmb_python_modules
  smtk
  paraview
  cinema_python
  pygments
  six
  vtk
  vtkmodules)

if (matplotlib_enabled)
  list(APPEND cmb_python_modules
    matplotlib)
endif ()

if (numpy_enabled)
  list(APPEND cmb_python_modules
    numpy)
endif ()

if (pythonmeshio_enabled)
  list(APPEND cmb_python_modules
    meshio)
endif ()

if (pythongirderclient_enabled)
  list(APPEND cmb_python_modules
    diskcache
    requests
    requests_toolbelt
    girder_client)
endif ()

if (paraviewweb_enabled)
  list(APPEND cmb_python_modules
    autobahn
    constantly
    incremental
    twisted
    wslink
    zope)

  if (WIN32)
    list(APPEND cmb_python_modules
      adodbapi
      isapi
      pythoncom
      win32com)
  endif ()
endif ()

################################################################################
# Qt5 plugins
################################################################################

if (qt5_enabled)
  include(qt5.functions)

  set(qt5_plugin_prefix)
  if (NOT WIN32)
    set(qt5_plugin_prefix "lib")
  endif ()

  # Add SVG support, so cmb can use paraview SVG icons.
  set(qt5_plugins
    iconengines/${qt5_plugin_prefix}qsvgicon
    imageformats/${qt5_plugin_prefix}qsvg
    sqldrivers/${qt5_plugin_prefix}qsqlite)

  if (WIN32)
    list(APPEND qt5_plugins
      platforms/qwindows)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/qwindowsvistastyle)
    endif ()
  elseif (APPLE)
    list(APPEND qt5_plugins
      platforms/libqcocoa
      printsupport/libcocoaprintersupport)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/libqmacstyle)
    endif ()
  elseif (UNIX)
    list(APPEND qt5_plugins
      platforms/libqxcb
      platforminputcontexts/libcomposeplatforminputcontextplugin
      xcbglintegrations/libqxcb-glx-integration)
  endif ()

  superbuild_install_qt5_plugin_paths(qt5_plugin_paths ${qt5_plugins})
else ()
  set(qt5_plugin_paths)
endif ()

################################################################################
# Data files
################################################################################

function (cmb_install_pdf project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/doc/${filename}"
      DESTINATION "${cmb_doc_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_data project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/examples/${filename}"
      DESTINATION "${cmb_example_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_extra_data)
  if (cmb_doc_dir)
    cmb_install_pdf(cmbusersguide "CMBUsersGuide.pdf")
    cmb_install_pdf(smtkusersguide "SMTKUsersGuide.pdf")
  endif ()
  if (cmb_example_dir)
    cmb_install_data(aevaexampledata "aeva-example.zip")
  endif()
endfunction ()
