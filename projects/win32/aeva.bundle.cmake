set(cmb_doc_dir "doc")
set(cmb_example_dir "examples")
set(plugin_dir "bin")

include(aeva.bundle.common)
include(cmb.bundle.windows)

# Install PDF guides.
cmb_install_extra_data()
