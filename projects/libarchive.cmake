set(libarchive_xml_dep)
set(libarchive_use_libxml2 OFF)
if (NOT WIN32)
  set(libarchive_xml_dep libxml2)
  set(libarchive_use_libxml2 ON)
endif ()

superbuild_add_project(libarchive
  DEPENDS bzip2 zlib ${libarchive_xml_dep}
  CMAKE_ARGS
    -DBUILD_TESTING:BOOL=OFF
    -DBUILD_TEST:BOOL=OFF
    -DENABLE_LIBXML2:BOOL=${libarchive_use_libxml2}
    -DENABLE_EXPAT:BOOL=OFF
    # quiet "developer-only" warnings
    -Wno-dev
    )

if (UNIX AND NOT APPLE)
  superbuild_add_extra_cmake_args(
    -DLibArchive_INCLUDE_DIR:PATH=<INSTALL_DIR>/include
    -DLibArchive_LIBRARY:FILEPATH=<INSTALL_DIR>/lib/${CMAKE_SHARED_LIBRARY_PREFIX}archive${CMAKE_SHARED_LIBRARY_SUFFIX}
    )
endif()
