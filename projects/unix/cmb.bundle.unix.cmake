set(library_paths
  "${superbuild_install_location}/lib"
  "${superbuild_install_location}/lib/paraview-${paraview_version}"
  "${superbuild_install_location}/lib/cmb-${cmb_version}")

if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../..")
endif ()

if (qt5_built_by_superbuild OR Qt5_DIR)
  list(APPEND include_regexes
    ".*/libicu.*.so.*")
endif ()

if (cumulus_enabled)
  # Include libcrypt from the packaging machine.
  list(APPEND include_regexes
    ".*/libcrypt.so.*")
endif ()

if (USE_SYSTEM_qt5 AND Qt5_DIR)
  list(APPEND include_regexes
    ".*/libQt5.*")
endif ()

foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  superbuild_unix_install_program("${superbuild_install_location}/bin/${executable}"
    "lib"
    SEARCH_DIRECTORIES "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

foreach (plugin_file IN LISTS all_plugin_files)
  # Get a list of plugins from this plugin file that need to be installed.
  get_filename_component(plugin_file_dir "${plugin_file}" DIRECTORY)
  foreach (plugin IN LISTS "plugins_${plugin_file}")
    # XXX(plugin): This should be `superbuild_unix_install_plugin`, but the binaries in
    # the CMB packages tend to link to no libraries which mean that no libraries get
    # installed to the main `lib` directory and the plugins then copy all of the libraries
    # into the plugin directory. Some way is needed to find the common libraries and keep
    # them in the `lib` directory should be found.
    superbuild_unix_install_module("${plugin}.so"
      "lib"
      "${plugin_file_dir}/${plugin}/"
      SEARCH_DIRECTORIES  "${library_paths}"
      LOCATION            "${plugin_file_dir}/${plugin}/"
      INCLUDE_REGEXES ${include_regexes}
      EXCLUDE_REGEXES ${exclude_regexes}
    )
  endforeach ()

  install(
    FILES       "${superbuild_install_location}/${plugin_file}"
    DESTINATION "${plugin_file_dir}"
    COMPONENT   "superbuild")
endforeach ()

foreach (library IN LISTS cmb_additional_libraries)
  superbuild_unix_install_module("lib${plugin}.so"
    "lib"
    "lib"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

superbuild_unix_install_python(
  LIBDIR              "lib"
  MODULES             ${cmb_python_modules}
  MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
                      "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES  "${library_paths}"
  INCLUDE_REGEXES ${include_regexes}
  EXCLUDE_REGEXES ${exclude_regexes})

if (cmb_install_paraview_python)
  superbuild_unix_install_python(
    LIBDIR              "lib"
    MODULES             paraview vtkmodules
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
                        "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})

  superbuild_unix_install_python(
    MODULE_DESTINATION  "/site-packages/paraview"
    LIBDIR              "lib"
    MODULES             vtk
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endif ()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/requests/cacert.pem"
    DESTINATION "lib/python${superbuild_python_version}/site-packages/requests"
    COMPONENT   superbuild)
endif ()

if (python2_enabled)
  include(python2.functions)
  superbuild_install_superbuild_python2()
elseif (python3_enabled)
  include(python3.functions)
  superbuild_install_superbuild_python3()
endif ()

if (paraviewweb_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages/paraview/web/defaultProxies.json"
    DESTINATION "lib/python${superbuild_python_version}/site-packages/paraview/web"
    COMPONENT   "${paraview_component}")
  install(
    DIRECTORY   "${superbuild_install_location}/share/paraview/web"
    DESTINATION "share/paraview-${paraview_version}"
    COMPONENT   "${paraview_component}")
endif ()

foreach (program_to_install IN LISTS cmb_programs_to_install)
  if (EXISTS "${superbuild_install_location}/bin/${program_to_install}.conf")
    install(
      FILES       "${superbuild_install_location}/bin/${program_to_install}.conf"
      DESTINATION "bin"
      COMPONENT   "superbuild")
  endif ()
endforeach ()

if (cmbworkflows_enabled)
  install(
    DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
    DESTINATION "share/cmb/workflows"
    COMPONENT   superbuild)
endif ()

if (qt5_enabled AND (NOT USE_SYSTEM_qt5 OR Qt5_DIR))
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/qt.conf" "[Paths]\nPrefix = ..\n")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/qt.conf"
    DESTINATION "bin"
    COMPONENT   superbuild)
endif ()

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_unix_install_module("${qt5_plugin_path}"
    "lib"
    "plugins/${qt5_plugin_group}/"
    LOADER_PATHS    "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    superbuild_unix_install_module("${superbuild_install_location}/meshkit/bin/${meshkit_exe}"
      "lib"
      "bin"
      SEARCH_DIRECTORIES "${library_paths}")
  endforeach ()
endif ()
