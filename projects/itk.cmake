if (DEFINED ENV{GITLAB_CI})
  # Inform ITK to allow all compilers to avoid clang-tidy errors:
  set(environment
    PROCESS_ENVIRONMENT ITK_ACCEPT_ALL_COMPILERS 1)
  message("Instructing ITK to accept all compilers")
endif()

superbuild_add_project(itk
  DEPENDS cxx11
  DEPENDS_OPTIONAL hdf5 vxl png zlib eigen
  ${environment}
  CMAKE_ARGS
    # Build what you need
    -DBUILD_EXAMPLES:BOOL=OFF
    -DBUILD_TESTING:BOOL=OFF
    -DITK_ACCEPT_ALL_COMPILERS:BOOL=ON
    -DITK_BUILD_DEFAULT_MODULES:BOOL=ON
    -DITK_BUILD_DOCUMENTATION:BOOL=OFF
    -DITK_WRAP_PYTHON:BOOL=OFF
    -DITK_SKIP_PATH_LENGTH_CHECKS:BOOL=ON
    # Using ITK doesn't work quite right.
    #-DITK_USE_SYSTEM_EIGEN:BOOL=${eigen_enabled}
    -DITK_USE_SYSTEM_HDF5:BOOL=${hdf5_enabled}
    -DITK_USE_SYSTEM_PNG:BOOL=${png_enabled}
    -DITK_USE_SYSTEM_VXL:BOOL=${vxl_enabled}
    -DITK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
)

set(itk_build_dir ${CMAKE_CURRENT_BINARY_DIR}/itk/build)
set(itk_install_dir <INSTALL_DIR>/lib/cmake/ITK-5.1)
