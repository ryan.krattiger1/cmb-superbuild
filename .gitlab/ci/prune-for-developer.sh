#!/bin/sh

set -e

# For each project to remove, clean its install artifacts.
for project in $PROJECTS_TO_CLEAN; do
    manifest="build/superbuild/$project/build/install_manifest.txt"
    sed -e "s,^$PREFIX/,," "$manifest" | tr -d $'\r' | \
        xargs \
            "--delimiter=\\n" \
            --max-args=40 \
            rm --force --verbose --dir
done

# Remove all pther build artifacts.
rm -rf build/superbuild
rm -rf build/cpack
rm -rf build/Testing
rm -rf build/CTest*
rm -rf build/DartConfiguration.tcl

# Create a tarball of the remaining build artifacts.
readonly date="$( date "+%Y%m%d" )"
readonly tarball="ci-$TARGET_PROJECT-ci-developer-$date-$CI_COMMIT_SHA.tar.gz"
tar czf "$tarball" build/

# Clean out the archived tree.
rm -rf build

# Move the tarball back to only make it visible for upload by the girder script.
mkdir build
mv -v "$tarball" build/
