include("${CMAKE_CURRENT_LIST_DIR}/gitlab_ci.cmake")

# Read the files from the build directory.
ctest_read_custom_files("${CTEST_BINARY_DIRECTORY}")

# Pick up from where the configure left off.
ctest_start(APPEND)

include(ProcessorCount)
ProcessorCount(nproc)

# Make files wanted by SMTK's testing.
file(MAKE_DIRECTORY "${CTEST_BINARY_DIRECTORY}/superbuild/smtk/build/Testing")
file(MAKE_DIRECTORY "${CTEST_BINARY_DIRECTORY}/superbuild/smtk/build/Testing/Temporary")

set(test_exclusions
  # Python3 support is missing in the ACE3P workflow.
  # https://gitlab.kitware.com/cmb/simulation-workflows/-/issues/2
  "TestSimExportOmega3P_01Py"
)

if ("$ENV{CI_JOB_NAME}" MATCHES "centos7")
  list(APPEND test_exclusions
    # OpenGL shader-using tests.
    "display(AuxiliaryGeometry|MultiBlockModel-test2D|ModelToMesh-simple)"
    "UnitTestRead(2dm|3dm|Obj|Pts|Stl|Vtp|Xyz)"
    "DeleteSmtkCell"
    "discreteImport2dmTest"
    "^package-import-test"
    "^pv\\."
    "RenderMesh"
    "unitQtComponentItem")
endif ()

if ("$ENV{CI_JOB_NAME}" MATCHES "windows")
  list(APPEND test_exclusions
    # Zoomed in somehow?
    "displayAuxiliaryGeometry"
    # Rendering changes.
    "displayModelToMesh-simple"
    # EGL context creation fails.
    "^pv\\."
    # Unknown SEGFAULT.
    "^ImportMultipleFiles$"
    # Heap corruption; valgrind does not find it.
    "^snapPointsToSurfacePy$"
    "^TestInterpolateAnnotatedValues$"
    "^TestSamplePointsOnSurface$"
    "^TestSelectionFootprint$"
    "^TestSnapPointsToSurface$"
    "^TestTransform$"
    "^TestTransformOp$")
endif ()

if ("$ENV{CI_JOB_NAME}" MATCHES "macos")
  list(APPEND test_exclusions
    # Rendering changes.
    "displayModelToMesh-simple"

    # Failing only on macOS. Needs investigation.
    "^pv\\.MeshSelection$")
endif ()

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()

ctest_test(
  PARALLEL_LEVEL "${nproc}"
  RETURN_VALUE test_result
  EXCLUDE "${test_exclusions}")
ctest_submit(PARTS Test)

if (test_result)
  message(FATAL_ERROR
    "Failed to test")
endif ()
