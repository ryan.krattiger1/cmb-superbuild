option(SUPERBUILD_SHALLOW_CLONES "Perform shallow clones rather than full clones" ON)
mark_as_advanced(SUPERBUILD_SHALLOW_CLONES)

superbuild_set_revision(boost
  URL     "https://www.paraview.org/files/dependencies/boost_1_71_0.tar.bz2"
  URL_MD5 4cdf9b5c2dc01fb2b7b733d5af30e558)
set(boost_no_junction_patch_necessary TRUE)

# While the goal is to be consistent with a SHA on ParaView's main fork, CMB
# frequently uses a custom fork of ParaView in order to maintain functionality
# while updating the connections between the two projects. When this happens, it
# is critical that the forked ParaView also push ParaView's tags to the fork,
# since this is how ParaView determines its version (and, resultantly, how it
# names its libraries).
#
# To push ParaView's tags:
#   $ cd <path-to-sb-build-tree>/superbuild/paraview/src
#   $ git remote add kitware https://gitlab.kitware.com/paraview/paraview.git
#   $ git fetch --tags kitware
#   $ git push --tags origin

superbuild_set_selectable_source(paraview
  SELECT git PROMOTE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/paraview/paraview.git"
    # XXX: When updating, use the date from the commit as shown in `git log`
    # and a short description of why the hash was updated. DO NOT BLINDLY
    # COPY THE COMMIT MESSAGE OF THE SHA. EXPLAIN WHY SMTK/CMB/AEVA NEED
    # THE NEW PARAVIEW VERSION.
    #
    # What: master @ Mon Feb 15 21:39:48 2021 +0000
    # Why:  Xcode 12.3 and Find module fixes from VTK/ParaView
    #       Also includes plugin location and context menu APIs
    GIT_TAG         "30de93d5053706afa747511809da5a9ef129c086"
  SELECT master CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/paraview/paraview.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT for-v6.3.0 CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/paraview.git"
    GIT_TAG         "2c64f2e43b873a399aa2513a24d336470f2cc3b4"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-paraview")

# VTK v9.0.1
superbuild_set_selectable_source(vtkonly
  SELECT release PROMOTE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/vtk/vtk.git"
    GIT_TAG        "v9.0.1"
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/vtk/vtk.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-vtkonly")

superbuild_set_selectable_source(cmb
  SELECT 6.3.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "v6.3.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-cmb")

superbuild_set_selectable_source(smtk
  SELECT 3.3.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v3.3.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-smtk")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG         "origin/master"
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(libarchive
  URL     "https://www.paraview.org/files/dependencies/libarchive-3.3.3.tar.gz"
  URL_MD5 4038e366ca5b659dae3efcc744e72120)

superbuild_set_revision(vxl
  GIT_REPOSITORY  "https://gitlab.kitware.com/third-party/vxl.git"
  GIT_TAG         origin/for/cmb
  GIT_SHALLOW     "ON")

superbuild_set_revision(itk
  GIT_REPOSITORY  "https://github.com/InsightSoftwareConsortium/ITK.git"
  # Accept newer HDF5:
  GIT_TAG         eb614464abc69544e13e2b6d851b89a786f635f1
  # Cannot checkout shallow copy of a hash
  # GIT_SHALLOW     "ON"
)

superbuild_set_revision(itkvtkglue
  GIT_REPOSITORY  "https://gitlab.kitware.com/aeva/itkvtkglue.git"
  GIT_TAG         origin/for-aeva)

# Use opencv from Thu Oct 6 13:40:33 2016 +0000
superbuild_set_revision(opencv
  # https://github.com/opencv/opencv.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/opencv-dd379ec9fddc1a1886766cf85844a6e18d38c4f1.tar.bz2"
  URL_MD5 19bbd14ed1bd741beccd6d19e444552f)

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  # https://github.com/robertmaynard/zeromq4-x.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/zeromq4-6d787cf69da6c69550e85a45be1bee1eb0e1c415-no-symlink.tar.bz2"
  URL_MD5 741e15aafbb9f98c48433186ddc484fa)

# Use remus from Thu Nov 7 16:05:31 2019 +0000
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        090a14c9a4b0e628a86590dcfa7e46ba728e9c04)

superbuild_set_revision(gdal
  # https://github.com/judajake/gdal-svn.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/gdal-98353693d6f1d607954220b2f8b040375e3d1744.tar.bz2"
  URL_MD5 5aa285dcc856f98ce44020ae1ae192cb)

superbuild_set_revision(eigen
  # https://github.com/eigenteam/eigen-git-mirror/releases/tag/3.3.7
  URL "https://www.computationalmodelbuilder.org/files/dependencies/eigen-git-mirror-3.3.7.tar.gz"
  URL_MD5 77a2c934eaf35943c43ee600a83b72df)

superbuild_set_revision(moab
  # https://bitbucket.org/fathomteam/moab.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/moab-6425a480ffe8e08b96453618fc5530e08e68ae8a.tar.bz2"
  URL_MD5 5946a405cddce972d54044fca6c87b11)

superbuild_set_revision(triangle
  # https://github.com/robertmaynard/triangle.git
  URL     "https://www.paraview.org/files/dependencies/triangle-4c20820448cdfa27f968cfd7cb33ea5b9426ad91.tar.bz2"
  URL_MD5 9a016bc90f1cdff441c75ceb53741b11)

superbuild_set_revision(pythonmeshio
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/meshio-4.3.11.tar.gz"
  URL_MD5 56593e77540413ceaccaecccd6b55585)

superbuild_set_revision(pythondiskcache
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/diskcache-3.1.0.tar.gz"
  URL_HASH SHA256=96cd1be1240257167a090794cce45db02ecf39d20b7a062580299b42107690ac)

superbuild_set_revision(pythongirderclient
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/girder-client-2.4.0.tar.gz"
  URL_HASH SHA256=1a9c882b8bce2e8233f572a4df9565c678e5614993e6606cdff04251532cddcb)

superbuild_set_revision(pythonrequests
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonrequeststoolbelt
  URL      "https://www.computationalmodelbuilder.org/files/dependencies/requests-toolbelt-0.8.0.tar.gz"
  URL_HASH SHA256=f6a531936c6fa4c6cfce1b9c10d5c4f498d16528d2a54a22ca00011205a187b5)

superbuild_set_revision(pybind11
  # https://github.com/pybind/pybind11.git
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/pybind11-97784dad3e518ccb415d5db57ff9b933495d9024.tar.bz2"
  URL_MD5 3e20adc18562f482b86bebaed19c55d2)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "https://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

superbuild_set_revision(occt
  # https://git.dev.opencascade.org/gitweb/?p=occt.git pick the tag you want, and download a snapshot.
  # current: 7.4.0p1
  # Extract, delete docs, tests, and sample data, and recompress as .tar.bz2
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/occt-7.4.0p1-stripped.tar.bz2"
  URL_MD5 24b95c5d55558ba087b9565f30c67718)

superbuild_set_revision(paraviewwebvisualizer
  URL     "https://www.paraview.org/files/dependencies/visualizer-2.0.12.tar.gz"
  URL_MD5 56e7e241ea6ad66b44469fc3186f47d6)

superbuild_set_revision(paraviewweblightviz
  URL     "https://www.paraview.org/files/dependencies/light-viz-1.16.1.tar.gz"
  URL_MD5 9ac1937cf07ae57bf85c3240f921679a)

superbuild_set_revision(protobuf
  GIT_REPOSITORY "https://github.com/protocolbuffers/protobuf"
  GIT_TAG         v3.8.0
  SOURCE_SUBDIR   cmake
  GIT_SHALLOW     "ON")

superbuild_set_revision(pyarc
  GIT_REPOSITORY  "git@kwgitlab.kitware.com:bob.obara/PyARC.git"
  GIT_TAG         origin/reorganization
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(cmbusersguide
  URL     "https://media.readthedocs.org/pdf/cmb/master/cmb.pdf")

superbuild_set_revision(smtkusersguide
  URL     "https://media.readthedocs.org/pdf/smtk/latest/smtk.pdf")

superbuild_set_revision(aevaexampledata
  URL     "https://data.kitware.com/api/v1/file/5fd8e2be2fa25629b9929a43/download/aeva-example.zip")

# Use json from Wed Mar 20 21:03:30 2019 +0100
superbuild_set_revision(nlohmannjson
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/json-295732a81780378c62d1c095078b4634dac8ec28.tar.bz2"
  URL_MD5 2113211f84a0b01c1c70900285c81e2c)

superbuild_set_revision(meshkit
  SOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/meshkit")

superbuild_set_revision(rggsession
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/plugins/rgg-session.git"
  GIT_TAG         origin/master
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(opencascadesession
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/plugins/opencascade-session.git"
  GIT_TAG         origin/master
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(smtkresourcemanagerstate
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/read-and-write-resource-manager-state.git"
  GIT_TAG        "origin/master")

superbuild_set_revision(smtkprojectmanager
  GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/plugins/project-manager.git"
  GIT_TAG         "origin/master"
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_revision(pegtl
  # https://github.com/taocpp/PEGTL/releases/tag/2.7.1
  URL "https://www.computationalmodelbuilder.org/files/dependencies/PEGTL-2.7.1.tar.gz"
  URL_MD5 da0363623cf0936e4cb2648b001af257)

superbuild_set_revision(las
  URL     "https://www.paraview.org/files/dependencies/libLAS-1.8.1.tar.bz2"
  URL_MD5 2e6a975dafdf57f59a385ccb87eb5919)

if (APPLE)
  set(capstone_file "Capstone_MacOsX_V912_Git_37e7ecd9b.dmg")
  set(capstone_md5 d7d64e784864295282e487930b4aafee)
elseif (NOT WIN32)
  set(capstone_file "Capstone_Redhat_7_6_V912_Git_37e7ecd.tar.gz")
  set(capstone_md5 5ecfd45eaa7b9b8109e72aa6fcfc0844)
endif ()

superbuild_set_revision(capstone
  URL "file://${KW_SHARE}/Projects/CMB/dependencies/Capstone-9.1.2/${capstone_file}"
  URL_MD5 "${capstone_md5}")

if (APPLE)
  set(cubit_file "Cubit-15.2-Mac64.dmg")
  set(cubit_md5 2bf9dedc8029b32040f06414ba3b88f9)
elseif (NOT WIN32)
  set(cubit_file "Cubit-15.2-Lin64.tar")
  set(cubit_md5 4f2cb33ac8eae693fdc289bc4ef172f4)
endif ()

superbuild_set_revision(cubit
  URL "file://${KW_SHARE}/Projects/CMB/dependencies/Cubit-15.2/${cubit_file}"
  URL_MD5 "${cubit_md5}")

superbuild_set_revision(aevasession
  GIT_REPOSITORY "https://gitlab.kitware.com/aeva/session.git"
  GIT_TAG origin/master
  GIT_SHALLOW "${SUPERBUILD_SHALLOW_CLONES}")

superbuild_set_selectable_source(aeva
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/aeva/aeva.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-aeva")

superbuild_set_revision(netgen
  URL     "https://www.computationalmodelbuilder.org/files/dependencies/netgen-v6.2.2007.tar.bz2"
  URL_MD5 "e4ce27d5cd07fb49177fc814a10d9d4d")
