include (SuperbuildTestingMacros)

set(modelbuilder_extract_root "${CMAKE_BINARY_DIR}/Testing/Temporary")

if (CMB_PACKAGE_FILE_NAME)
  set(glob_prefix "${CMB_PACKAGE_FILE_NAME}")
else ()
  set(glob_prefix "modelbuilder-*")
  if (CMB_PACKAGE_SUFFIX)
    set(glob_prefix "${glob_prefix}-${CMB_PACKAGE_SUFFIX}")
  endif ()
endif ()

#[==[.md
Test that extracts modelbuilder package file

```
add_package_extract_test(generator)
```
This test calls the superbuild_add_extract_test to extract the modelbuilder
package file.

#]==]
function (add_package_extract_test  generator)
  set(extract_dir "${modelbuilder_extract_root}/modelbuilder-${generator}/test-extracted")
  superbuild_add_extract_test("modelbuilder" "${glob_prefix}" "${generator}" "${extract_dir}")
endfunction ()

#[==[.md
Tests that extracted modelbuilder executable can import smtk python modules.

```
add_package_import_test(generator)
```
This test runs modelbuilder and checks if it can import a minimal smtk operation,
which requires the python environment to be configured correctly.

  * The test command runs a cmake script that runs the modelbuilder executable.
  * Variables passed to the cmake script specify the paths to the executable,
    the xml script, and other needed info.
  * The xml script invokes the modelbuilder "Import Python Operation..." menu
    item and selects a python file "minimum_operation.py" in the scripts
    subfolder).
  * If the import operation is successful, a small text file is generated in
    the test directory.

#]==]
function (add_package_import_test  generator)
  set(extract_dir "${modelbuilder_extract_root}/modelbuilder-${generator}/test-extracted")
  set(this_dir "${CMAKE_SOURCE_DIR}/cmake")
  set(test_dir "${CMAKE_BINARY_DIR}/Testing/Temporary")

  set(exe_path "bin/modelbuilder")
  if (APPLE)
    set(exe_path "modelbuilder.app/Contents/MacOS/modelbuilder")
  endif ()

  add_test(
    NAME "package-import-test-${generator}"
    COMMAND
      "${CMAKE_COMMAND}"
      "-DEXECUTABLE=${extract_dir}/${exe_path}"
      "-DEXPECTED_FILE=${test_dir}/minimum_operation.txt"
      "-DDATA_DIR=${this_dir}/scripts"
      "-DTEST_DIR=${test_dir}"
      "-DTEST_SCRIPT=${this_dir}/scripts/import_minimum_operation.xml"
      "-P" "${this_dir}/import_pythonop.cmake"
    WORKING_DIRECTORY "${test_dir}"
    )

  set_tests_properties("package-import-test-${generator}"
    PROPERTIES
      ${ARGN}
    )
endfunction ()
