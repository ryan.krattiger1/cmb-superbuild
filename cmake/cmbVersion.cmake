string(REPLACE "origin/" "" cmb_tag "${cmb_GIT_TAG}")
string(REPLACE ".git" "/raw/${cmb_tag}/version.txt" cmb_version_url "${cmb_GIT_REPOSITORY}")

# Download CMB's master branch version.txt
file(DOWNLOAD "${cmb_version_url}"
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cmbVersion.txt STATUS status)
list(GET status 0 error_code)
if (error_code)
  file(READ "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cmbVersion.txt" output)
  message(FATAL_ERROR "Could not access the version file for CMB: ${output}")
endif ()

file(STRINGS ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cmbVersion.txt version_string )

string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
  version_matches "${version_string}")

set(cmb_version_major ${CMAKE_MATCH_1})
set(cmb_version_minor ${CMAKE_MATCH_2})
set(cmb_version_patch "${CMAKE_MATCH_3}")
# Do we just have a patch version or are there extra stuff?
if (CMAKE_MATCH_4)
  set(cmb_version_patch "${CMAKE_MATCH_3}-${CMAKE_MATCH_4}")
endif()
set(cmb_version "${cmb_version_major}.${cmb_version_minor}")
