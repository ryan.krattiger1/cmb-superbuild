# Script for running modelbuilder test script, expected it to generate
# a file as a result.
# Requred variables:
#   EXECUTABLE: path to the modelbuilder executable
#   EXPECTED_FILE: path to the file that modelbuilder generates
#   DATA_DIR: argument to pass to --data-directory
#   TEST_DIR: argument to pass to --test-directory
#   TEST_SCRIPT: path to the test (.xml) file to play

# Remove expected file before running modelbuilder
if (EXISTS ${EXPECTED_FILE})
  file(REMOVE ${EXPECTED_FILE})
endif ()

# Run modelbuilder
execute_process(
  COMMAND
    ${EXECUTABLE}
    "-dr"
    "--data-directory=${DATA_DIR}"
    "--test-directory=${TEST_DIR}"
    "--test-script=${TEST_SCRIPT}"
    "--exit"
  RESULT_VARIABLE error_code
)

if (error_code)
  message(FATAL_ERROR "Modelbuilder returned error code ${error_code}.")
endif ()

# Check if expected file was generated
if (NOT EXISTS ${EXPECTED_FILE})
  message(FATAL_ERROR "The expected file (${EXPECTED_FILE}) was not generated.")
endif ()
