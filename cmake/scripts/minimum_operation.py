"""A minimum pythonn operation to verify that smtk python modules
   can be imported and operations loaded.
"""


import os

import smtk
import smtk.attribute
import smtk.common
import smtk.io
import smtk.mesh
import smtk.model
import smtk.operation
import smtk.resource
import smtk.simulation
import smtk.view


class MinimumOperation(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "Minimal Operation"

    def operateInternal(self):
        outcome = int(smtk.operation.Operation.Outcome.SUCCEEDED)
        result = self.createResult(outcome)
        return result

    def createSpecification(self):
        spec = self.createBaseSpecification()
        return spec

# Write small text file, which is used to confirm that import was successful.
wd = os.getcwd()
filename = 'minimum_operation.txt'
path = os.path.join(wd, filename)
with open(path, 'w') as f2:
    f2.write(filename)
    print('Wrote', path)
